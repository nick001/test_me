from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template import RequestContext, loader, Context
from django.middleware import csrf
from django.conf import settings
from PIL import Image
import json
import datetime

from test_me.forms import Login, Registration, MakePost, MakeCategory, MakeCountry, UploadPictures
from test_me.models import User, Post, AuthUser, Category, Country, UploadedFile

my_ip_address = '10.10.2.52'
host_name = 'http://' + my_ip_address + '/test_me/'

def readPost(request, post_pk):
    if request.method == 'POST' and request.is_ajax():
        command = str(request.POST.get('command', ''))
        if command == 'delete_post':
            return deletePost(request)
        if command == 'edit_post':
            return makePost(request, int(request.POST.get('post_pk', '')))
        if command == 'add_to_favorite':
            return addToFavorite(request)
        if command == 'approve_post':
            return approvePost(request)
        if command == 'delete_from_favorite':
            return deleteFromFavorite(request)

    token = csrf.get_token(request)
    current_post = Post.objects.get(pk = post_pk)
    user_status = getUserStatus(token)
    if user_status != 'admin':
        if current_post.author == isLoggedIn(token):
            user_status = 'author'

    pictures = []
    for picture_pk in current_post.picture.split(','):
        if picture_pk == '':
            continue
        corrected_picture = UploadedFile.objects.get(pk = picture_pk).file_location
        pictures.append(corrected_picture)

    template = loader.get_template('test_me/read_post.html')
    context = RequestContext(request, {'post' : current_post, 'user_status' : user_status, 'pictures' : pictures})
    return HttpResponse(template.render(context))

def makePost(request, post_pk = '0', picture_pk = '0'):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if getUserStatus(token) == 'ban':
        return(JsonResponse({'error_message' : 'you are banned'}))

    if post_pk == '0':
        post = Post(
            header = 'header there',
            subheader = 'subheader there',
            category = '',
            body = 'body there',
            picture = '',
            author = isLoggedIn(token),
            status = 'editing',
            date_of_last_edit = '',
            date_of_post = ''
        )
        post.save()
        return redirect(host_name + 'make_post/' + str(post.pk) + '/0')
    else:
        post = Post.objects.get(pk = post_pk)

    if picture_pk != '0':
        post = Post.objects.get(pk = post_pk)
        post.picture = post.picture + ', ' + str(picture_pk)
        post.save()

    if request.method == 'POST' and request.is_ajax():

        if request.POST.get('command', '') == 'edit_post':
            post = Post.objects.get(pk = post_pk)
            if str(isLoggedIn(token)) != post.author:
                return(JsonResponse({'error_message' : 'not your post ' + str(isLoggedIn(token)) + ' ' + post.author}))

            return(JsonResponse({'redirect_url' : host_name + 'make_post/' + str(post_pk) + '/0'}))

        if request.POST.get('command', '') == 'delete_picture':
            post = Post.objects.get(pk = post_pk)
            if str(isLoggedIn(token)) != post.author:
                return(JsonResponse({'error_message' : 'not your post ' + str(isLoggedIn(token)) + ' ' + post.author}))
            picture_pk = request.POST.get('picture_pk', '')
            post.picture = post.picture.replace(str(picture_pk) + ',', '')
            post.save()
            return(JsonResponse({'redirect_url' : host_name + 'make_post/' + str(post_pk) + '/0'}))

        form = MakePost(request.POST)

        if form.is_valid():
            pk = isLoggedIn(token)
            current_date = str(datetime.datetime.now())
            post.header = form.cleaned_data['header']
            post.subheader = form.cleaned_data['subheader']
            post.category = request.POST.get('category', '')
            post.body = form.cleaned_data['body']
            post.author = str(pk)
            post.status = 'moderating'
            post.date_of_last_edit = current_date
            post.date_of_post = current_date
            post.save()
            return(JsonResponse({'redirect_url' : host_name + 'posts/all/'}))
        else:
            return(JsonResponse({'error_message' : 'got trouble in makePost'}))

    categories = Category.objects.all()
    form = UploadPictures
    template = loader.get_template('test_me/make_post.html')

    pictures = []
    for picture_pk in post.picture.split(','):
        if picture_pk == '':
            continue
        corrected_picture = UploadedFile.objects.get(pk = picture_pk).file_location
        pictures.append([corrected_picture, picture_pk])

    upload_link = host_name + 'upload_picture/' + str(post_pk)
    context = RequestContext(request, {'categories' : categories, 'upload_form' : form, 'new_post' : post, 'pictures' : pictures, 'upload_link' : upload_link}, processors = [makePostProc])
    return HttpResponse(template.render(context))

def approvePost(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return(JsonResponse({'redirect_url' : host_name + 'login/'}))

    if getUserStatus(token) != 'admin':
        return(JsonResponse({'redirect_url' : host_name + 'post/all/'}))

    if request.method == 'POST' and request.is_ajax():
        post_pk = request.POST.get('post_pk', '')
        current_post = Post.objects.get(pk = int(post_pk))
        current_post.status = 'approved'
        current_post.save()
        return(JsonResponse({'error_message' : 'post approved', 'redirect_url' : host_name + 'read_post/' + str(post_pk)}))

    return redirect(host_name)

def deletePost(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if request.method == 'POST' and request.is_ajax():
        post_pk = request.POST.get('post_pk', '')
        current_post = Post.objects.get(pk = int(post_pk))
        user_status = getUserStatus(token)
        if user_status == 'admin':
            Post.objects.filter(pk = post_pk).delete()
            return(JsonResponse({'redirect_url' : host_name + 'posts/all/'}))
        else:
            if current_post.author == isLoggedIn(token):
                Post.objects.filter(pk = post_pk).delete()
                return(JsonResponse({'redirect_url' : host_name + 'posts/all/'}))
            else:
                return(JsonResponse({'redirect_url' : host_name + 'posts/all/'}))

    return redirect(host_name)

def logout(request):
    global host_name
    token = csrf.get_token(request)
    if token:
        logoutUser(token)

    return redirect(host_name + 'login/')

def logoutUser(token):
    AuthUser.objects.filter(token = token).delete()

def moderateUsers(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if getUserStatus(token) != 'admin':
        return redirect(host_name + 'login/')

    if request.method == 'POST' and request.is_ajax():
        user_pk = request.POST.get('user_pk', '')
        new_user_status = request.POST.get('new_status', '')
        user_to_change = User.objects.get(pk = user_pk)
        user_to_change.status = new_user_status
        user_to_change.save()
        return(JsonResponse({'redirect_url' : host_name + 'moderate_users/'}))

    all_users = User.objects.all()
    template = loader.get_template('test_me/moderate_users.html')
    context = RequestContext(request, {'users' : all_users})
    return HttpResponse(template.render(context)) 

def admin(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if getUserStatus(token) != 'admin':
        return redirect(host_name + 'login/')

    template = loader.get_template('test_me/admin.html')
    context = RequestContext(request)
    return HttpResponse(template.render(context)) 

def addToFavorite(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if request.method == 'POST' and request.is_ajax():
        post_pk = request.POST.get('pk', '')
        current_user = User.objects.get(pk = isLoggedIn(token))
        updated_favorites = current_user.favorites + ', ' +str(post_pk)
        current_user.favorites = updated_favorites
        current_user.save()
        return(JsonResponse({'error_message' : 'added to favorite'}))

    template = loader.get_template('test_me/posts.html')
    context = RequestContext(request)
    return HttpResponse(template.render(context))   

def deleteFromFavorite(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if request.method == 'POST' and request.is_ajax():
        post_pk = request.POST.get('pk', '')
        current_user = User.objects.get(pk = isLoggedIn(token))
        updated_favorites = clearFavorites(current_user.favorites.split(','))
        if post_pk in updated_favorites:
            updated_favorites.remove(post_pk)
        current_user.favorites = updated_favorites
        current_user.save()
        return(JsonResponse({'error_message' : 'deleted from favorite'}))

    template = loader.get_template('test_me/posts.html')
    context = RequestContext(request)
    return HttpResponse(template.render(context)) 

def makeCountry(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if getUserStatus(token) != 'admin':
        template = loader.get_template('test_me/urntadmin.html')
        context = RequestContext(request)
        return HttpResponse(template.render(context))

    if request.method == 'POST' and request.is_ajax():
        form = MakeCountry(request.POST)
        if form.is_valid():
            new_country = Country(
                name = form.cleaned_data['name'],
                cities = form.cleaned_data['cities']
            )
            new_country.save()
            return(JsonResponse({'redirect_url' : host_name + 'posts/all/'}))
        else:
            return(JsonResponse({'error_message' : 'got trouble'}))

    template = loader.get_template('test_me/make_country.html')
    context = RequestContext(request, processors = [makeCountryProc])
    return HttpResponse(template.render(context))    

def readCategory(request, category_pk):
    current_posts = Category.objects.get(pk = int(category_pk)).posts

    template = loader.get_template('test_me/read_category.html')
    context = RequestContext(request, {'post' : current_post})
    return HttpResponse(template.render(context))

def makeCategory(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')

    if getUserStatus(token) != 'admin':
        template = loader.get_template('test_me/urntadmin.html')
        context = RequestContext(request)
        return HttpResponse(template.render(context))

    if request.method == 'POST' and request.is_ajax():
        form = MakeCategory(request.POST)
        if form.is_valid():
            new_category = Category(
                name = form.cleaned_data['name'],
                annotation = form.cleaned_data['annotation'],
                posts = '0'
            )
            new_category.save()
            return(JsonResponse({'redirect_url' : host_name + 'categories/'}))
        else:
            return(JsonResponse({'error_message' : 'got trouble'}))

    template = loader.get_template('test_me/make_category.html')
    context = RequestContext(request, processors = [makeCategoryProc])
    return HttpResponse(template.render(context))

def showCategories(request):
    token = csrf.get_token(request)
    status = getUserStatus(token)
    all_categories = Category.objects.all()

    template = loader.get_template('test_me/categories.html')
    context = RequestContext(request, {'user_status' : status, 'all_categories' : all_categories})
    return HttpResponse(template.render(context))

def posts(request, category = 'all'):
    token = csrf.get_token(request)
    status = getUserStatus(token)

    all_posts = list(Post.objects.all())
    if category == 'all':
        posts = all_posts
    elif category == 'my_favorites':
        posts = []
        current_user = User.objects.get(pk = isLoggedIn(token))
        cleared_favorites = clearFavorites(current_user.favorites.split(','))
        current_user.favorites = str(cleared_favorites)
        current_user.save()
        for value in cleared_favorites:
            posts.append(Post.objects.get(pk = int(value)))
    elif category == 'not_approved':
        if getUserStatus(token) == 'admin':
            posts = []
            for post in all_posts:
                if post.status == 'moderating':
                    posts.append(post)
        else:
            posts = all_posts
    else:
        posts = []
        for post in all_posts:
            if post.category == category:
                posts.append(post)

    if getUserStatus(token) != 'admin':
        for post in posts:
            if post.status != 'approved':
                posts.remove(post)

    posts = addMini(posts)

    template = loader.get_template('test_me/posts.html')
    context = RequestContext(request, {'user_status' : status, 'posts' : posts, 'category' : category})
    return HttpResponse(template.render(context))

def addMini(posts):
    new_posts = []
    for post in posts:
        tmp = post.picture.split(',')
        if len(tmp) > 1:
            picture_pk = int(tmp[1])
            mini_picture = UploadedFile.objects.get(pk = picture_pk).file_location
        else:
            mini_picture = 'empty'
        new_posts.append([post, mini_picture])
    return (new_posts)

def clearFavorites(user_favorites):
    if '' in user_favorites:
        user_favorites.remove('')
    return (list(set(user_favorites)))

def getUserStatus(token):
    pk = isLoggedIn(token)
    if not pk:
        status = 'guest'
    else:
        status = User.objects.get(pk = pk).status
    return(status)

def getUserName(pk):
    return(User.objects.get(pk = pk).name)

def index(request):
    global host_name
    token = csrf.get_token(request)
    if not isLoggedIn(token):
        return redirect(host_name + 'login/')
    else:
        return redirect(host_name + 'posts/all/')

def register(request):
    global host_name
    if request.method == 'POST' and request.is_ajax():
        form = Registration(request.POST)
        if form.is_valid():
            new_user_name = form.cleaned_data['name']
            new_user_email = form.cleaned_data['email']
            new_user_password = form.cleaned_data['password']
            new_user_country = request.POST.get('country', '')
            new_user_city = request.POST.get('city', '')
            try:
                if User.objects.get(name = new_user_name):
                    return(JsonResponse({'error_message' : 'nick_name already exsist'}))
                if User.objects.get(email = new_user_email):
                    return(JsonResponse({'error_message' : 'email already exsist'}))
            except:
                pass

            new_user = User(
                name = new_user_name,
                email = new_user_email,
                password = new_user_password,
                country = new_user_country,
                city = new_user_city,
                status = 'admin',
                posts = '0',
                registration_date = str(datetime.datetime.now())
            )
            new_user.save()
####                REDIRECT!!!
            token = csrf.get_token(request)
            loginUser(new_user.pk, token)
            return(JsonResponse({'redirect_url' : host_name + 'posts/all'}))
####                REDIRECT!!!
    all_countries = Country.objects.all()
    template = loader.get_template('test_me/registration.html')

    TMP = []
    for country in all_countries:
        tmp_voc = {
            'name' : str(country.name),
            'cities' : str(country.cities)
        }
        TMP.append(tmp_voc)

    context = RequestContext(request, {'countries' : TMP}, processors = [registrationProc])
    return HttpResponse(template.render(context))

def login(request):
    global host_name
    token = csrf.get_token(request)
    if isLoggedIn(token):
        return redirect(host_name + 'posts/all/')

    if request.method == 'POST' and request.is_ajax():
        form = Login(request.POST)
        if form.is_valid():
            user_name_to_check = form.cleaned_data['name']
            password_to_check = form.cleaned_data['password']

            users = User.objects.all()
            for user in users:
                if user.name == user_name_to_check:
                    if user.password == password_to_check:
                        loginUser(user.pk, token)
                        return(JsonResponse({'redirect_url' : host_name + 'posts/all'}))
                    else:
                        return(JsonResponse({'error_message' : str(user.password) + '<p></p>' + str(password_to_check) + str(token)}))
            return(JsonResponse({'error_message' : 'no user'}))

    template = loader.get_template('test_me/login.html')
    context = RequestContext(request, processors = [loginProc])
    return HttpResponse(template.render(context))

def loginUser(pk, token):
    current_users = AuthUser.objects.all()
    for user in current_users:
        if str(user.pk) == pk:
            user.token = token
            user.save()
            return()
    new_auth_user = AuthUser(token = token)
    new_auth_user.pk = pk
    new_auth_user.save()
    return()

def loginProc(request):
    return {
       'form' : Login(request.POST),
    }

def registrationProc(request):
    return {
       'form' : Registration(request.POST),
    }

def makePostProc(request):
    return {
       'form' : MakePost(request.POST),
    }

def makeCategoryProc(request):
    return {
       'form' : MakeCategory(request.POST),
    }

def makeCountryProc(request):
    return {
       'form' : MakeCountry(request.POST),
    }

def isLoggedIn(token):
    current_users = AuthUser.objects.all()
    for user in current_users:
        if user.token == token:
            return(user.pk)
    return(False)

def uploadPicture(request, post_pk = 0):
    token = csrf.get_token(request)
    if request.method == 'POST':
        form = UploadPictures(request.POST, request.FILES)
        if form.is_valid():
            uploaded_picture = request.FILES['picture']
            file_type = checkPicture(uploaded_picture)
            if file_type:
                file_name = str(uploaded_picture).split('.')[0] + '.' + file_type
                new_picture = UploadedFile(
                    user_pk = isLoggedIn(token),
                    name = file_name,
                    file_type = file_type,
                    file_location = uploaded_picture
                )
                new_picture.save()
                file_name = str(settings.MEDIA_ROOT) + '/' + str(new_picture.file_location)
                correctImage(file_name, file_type, 1024, 768)
                return redirect(host_name + 'make_post/' + str(post_pk) + '/' + str(new_picture.pk))
            else:
                return (JsonResponse({'error_message' : 'error in uploadPicture', 'redirect_url' : host_name + 'make_post/0/0'}))
    return redirect(host_name + 'make_post/' + str(post_pk) + '/0')

def correctImage(file_name, file_type, size_x, size_y):
    picture = Image.open(file_name)

    if size_x < picture.size[0] or size_y < picture.size[1]:
        picture.thumbnail((size_x, size_y), Image.ANTIALIAS)

    picture.save(file_name, file_type)

def checkPicture(picture):
    file_type = picture.content_type.split('/')[1]
    
    if file_type != 'jpeg' and file_type != 'gif' and file_type != 'jpg':
        return False

    if len(picture.name.split('.')) == 1:
        return False

    return file_type
