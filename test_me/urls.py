from django.conf.urls import url

from django.conf import settings
from django.conf.urls.static import static

from test_me import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^registration', views.register),
    url(r'^login', views.login),
    url(r'^logout', views.logout),
    url(r'^categories', views.showCategories),
    url(r'^posts/(?P<category>\w+)', views.posts),
    url(r'^make_post/(?P<post_pk>\w+)/(?P<picture_pk>\w+)', views.makePost),
    url(r'^upload_picture/(?P<post_pk>\w+)', views.uploadPicture, name = 'upload_form'),
    url(r'^make_category', views.makeCategory),
    url(r'^read_post/(?P<post_pk>\w+)', views.readPost),
    url(r'^read_category/([0-9])', views.readCategory),
    url(r'^make_country', views.makeCountry),
    url(r'^admin', views.admin),
    url(r'^moderate_users', views.moderateUsers),
]
