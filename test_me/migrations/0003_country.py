# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-07-01 11:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_me', '0002_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
                ('cities', models.CharField(max_length=1000)),
            ],
        ),
    ]
