# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-06-30 22:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header', models.CharField(max_length=40)),
                ('subheader', models.CharField(max_length=40)),
                ('category', models.CharField(max_length=40)),
                ('body', models.CharField(max_length=1000)),
                ('pictures', models.CharField(max_length=200)),
                ('author', models.CharField(max_length=40)),
                ('status', models.CharField(max_length=40)),
                ('date_of_last_edit', models.CharField(max_length=40)),
                ('date_of_post', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40)),
                ('email', models.CharField(max_length=40)),
                ('password', models.CharField(max_length=40)),
                ('country', models.CharField(max_length=40)),
                ('city', models.CharField(max_length=40)),
                ('status', models.CharField(max_length=40)),
                ('registration_date', models.CharField(max_length=40)),
                ('posts', models.CharField(max_length=100)),
                ('favorites', models.CharField(max_length=100)),
            ],
        ),
    ]
