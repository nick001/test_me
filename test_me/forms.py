from django import forms

class Registration(forms.Form):
    name = forms.CharField(label = 'name', max_length = 40)
    email = forms.CharField(label = 'email', max_length = 40)
    password = forms.CharField(label = 'password', max_length = 40)

class Login(forms.Form):
    name = forms.CharField(label = 'name', max_length = 40)
    password = forms.CharField(label = 'password', max_length = 40)

class MakePost(forms.Form):
    header = forms.CharField(label = 'header', max_length = 40)
    subheader = forms.CharField(label = 'subheader', max_length = 40)
    body = forms.CharField(label = 'body', max_length = 1000)

class UploadPictures(forms.Form):
    picture = forms.FileField(label = 'picture', help_text = 'only JPEG, GIF', widget = forms.ClearableFileInput(attrs = {'multiple' : True}))

class MakeCategory(forms.Form):
    name = forms.CharField(label = 'name', max_length = 40)
    annotation = forms.CharField(label = 'annotation', max_length = 200)

class MakeCountry(forms.Form):
    name = forms.CharField(label = 'name', max_length = 40)
    cities = forms.CharField(label = 'cities', max_length = 2000)
