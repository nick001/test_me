from django.db import models

class User(models.Model):
    name = models.CharField(max_length = 40)
    email = models.CharField(max_length = 40)
    password = models.CharField(max_length = 40)
    country = models.CharField(max_length = 40)
    city = models.CharField(max_length = 40)
    status = models.CharField(max_length = 40)
    registration_date = models.CharField(max_length = 40)
    posts = models.CharField(max_length = 100)
    favorites = models.CharField(max_length = 100)

class AuthUser(models.Model):
    token = models.CharField(max_length = 40)

class Post(models.Model):
    header = models.CharField(max_length = 40)
    subheader = models.CharField(max_length = 40)
    category = models.CharField(max_length = 40)
    body = models.CharField(max_length = 1000)
    picture = models.CharField(max_length = 200)
    author = models.CharField(max_length = 40)
    status = models.CharField(max_length = 40)
    date_of_last_edit = models.CharField(max_length = 40)
    date_of_post = models.CharField(max_length = 40)

class Category(models.Model):
    name = models.CharField(max_length = 40)
    annotation = models.CharField(max_length = 200)
    posts = models.CharField(max_length = 200)

class Country(models.Model):
    name = models.CharField(max_length = 40)
    cities = models.CharField(max_length = 1000)

def user_directory_path(instance, filename):
    return 'test_me/user_{0}/{1}'.format(instance.user_pk, filename)

class UploadedFile(models.Model):
    user_pk = models.CharField(max_length = 40)
    name = models.CharField(max_length = 40)
    file_type = models.CharField(max_length = 40)
    file_location = models.FileField(upload_to = user_directory_path)
